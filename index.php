<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" 
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>POST voorbeeld</title>
    
    <link href="css/style.css" rel="stylesheet" type="text/css" />
   
    <script type="text/javascript"src="js/utils/util.js"></script>
    <script type="text/javascript"src="js/utils/request.js"></script>
	<script type="text/javascript"src="js/utils/events.js"></script>
	<script type="text/javascript"src="js/jquery/jquery.js"></script>
	
	<script type="text/javascript"src="js/form.js"></script>
		
</head>
<body>
    <form method="post" action="xhr/formactie.php" id="form">
    <fieldset>
		
		<legend>een voorbeeld formulier:</legend>
		
		<p>
			<span>naam:</span> 		<input type="text" name="naam" value="" />
						<input type="button" rel="naam" name="save" value="save"/><br />
			<span>adres:</span> 		<input type="text" name="adres" value="" />
						<input type="button" rel="adres" name="save" value="save"/><br />
			<span>plaats:</span> 	<input type="text" name="plaats" value="" />
						<input type="button" rel="plaats" name="save" value="save"/><br />
			<span>land:</span> 		<input type="text" name="land" value="" />
						<input type="button" rel="land" name="save" value="save"/><br />
			<span>postcode:</span> 	<input type="text" name="postcode" value="" />
						<input type="button" rel="postcode" name="save" value="save"/><br />
			<span>telefoon:</span> 	<input type="text" name="telefoon" value="" />
						<input type="button" rel="telefoon" name="save" value="save"/><br />
			<span>e-mail:</span> 	<input type="text" name="email" value="" />
						<input type="button" rel="email" name="save" value="save"/><br />
			<span>mening :</span> 	<input type="radio" name="mening" value="ja" /> ja
						<input type="radio" name="mening" value="nee" /> nee
						<input type="radio" name="mening" value="weetniet" /> weet niet
						<input type="radio" name="mening" value="geeninteresse" /> geen interesse
						<input type="button" rel="mening" name="save" value="save"/>
						<br>
			<span>lid :</span> 		<input type="checkbox" name="lid" value="ja"/>
						<input type="button" rel="lid" name="save" value="save"/>
		</p>
		<input type="hidden" name="hidden" value="id" />
		<p>
			<input type="submit" name="opslaan" value="Gegevens opslaan" />
		</p>
		<p>
			<input type="button" name="btn" value="submit form"/>
		</p>
		<p>
			<input type="button" name="btn_exept" value="submit form with exeptions"/>
		</p>
		<p>
			<input type="button" name="btn_include" value="submit form with inclusions"/>
		</p>
		<div id="melding" style="display:none;"></div>
		<div id="alert" style="display:none;"></div>
		</fieldset>
		<br><br>
		<fieldset>
		<legend>Actions</legend>
		<input type="button" value="start polling" onclick="$.startPolling();" />
		<input type="button" value="stop polling" onclick="$.stopPolling();" />
		<input type="button" value="send poll" onclick="$.sendPoll();" />
		<input type="button" value="clear poll" onclick="$.clearPoll();" />
		<input type="button" value="add polldummy" onclick="frm.addPollDummy();" />
		<input type="button" value="set interval" onclick="$.setPollingInterval($.getVal('interval'));" />
		<input type="text" size="5" name="interval" id="interval" value=""/>&nbsp;ms, default 4000 ms
		</fieldset>
		<br><br>
		
	</form>
</body>
</html>
