<?php
if ( !function_exists('json_decode') ){
    function json_decode($content, $assoc=true){
                require_once 'class/json.php';
                if ( $assoc ){
                    $json = new Services_JSON(SERVICES_JSON_LOOSE_TYPE);
        } else {
                    $json = new Services_JSON;
                }
        return $json->decode($content);
    }
}

if ( !function_exists('json_encode') ){
    function json_encode($content){
                require_once 'class/json.php';
                $json = new Services_JSON;
               
        return $json->encode($content);
    }
}

function getExt($url) {
	$allowed= array('.pdf','.doc','jpeg','.jpg','.png','.gif','.xls','docx','.mp3');
	$extent=substr(strrchr($url, "."), -4,4);
	if(in_array($extent,$allowed))
	{
	$extent='['.$extent.']';
	return $extent;
	}
	else{return false;}
}
?>