var frm = (function(){
	
			//PRIVATE METHODS EN VARS
			
			//private constants
			var CNT_DISPLAY = 'melding',
				CNT_ALERT = 'alert',
				MSG_ERROR = 'Dit is een custom foutmelding',
				MSG_SUCCESS = 'Dit is een custom succes-melding',
				MSG_OK='Request voltooid, ',
				SHOWTIME=3000;//tijd dat message wordt getoond, als SHOWTIME=false dan blijft de message staan tot er op geklikt wordt
			
			var bRequestActive=false,//true als er nog XHR-request gaande is
				oForm,//referentie naar het  formulier, zonder declaratie wordt oForm global
				oConnect = new objEventTarget();//event-interface object
			
			var handler =  {
			
				process : function(event) {
					var t=event.target;
					switch(event.type)
					{
						case 'click' :
							//eventueel hier clientside validatie
							if(t.name == 'opslaan') { o.frmSubmit(); event.preventDefault();}
							if(t.name == 'btn') { o.frmSubmit();}
							
							if(t.name == 'btn_exept') { o.frmSubmit({
								"naam"			: false,
								"adres"			: false
								
							});}
							if(t.name == 'btn_include') { o.frmSubmit({
								//include elements
								"inclusion"		: true,
								"naam"			: true,
								"adres"			: true
							});}
							
							if(t.id == CNT_DISPLAY) { $('#'+CNT_DISPLAY).fadeOut('slow');}//klik op display om te verbergen
							if(t.name == 'save') { o.frmElementSave(t);}
						break;
					}
				}
			};
			
			var output = { //XHR-interface
			
				setResult : function(oResponse, sStatus) {
					
					bRequestActive=false;
					$.setHTML(CNT_DISPLAY,MSG_OK + oResponse.data + oResponse.tId +' -> ' + sStatus);
					$.addClass(CNT_DISPLAY,'success');
					$.doShow(CNT_DISPLAY,SHOWTIME);
				},
				
				setNotify : function(oResponse, sStatus) {
					
					bRequestActive=false;
					$.setHTML(CNT_ALERT,oResponse.data + oResponse.tId +' -> ' + sStatus);
					$.doShow(CNT_ALERT,SHOWTIME);
				}
			};
			
			var XHR = {
				//req 01-----------------------------------------
				doSubmit : function(obj){
				
					$.getForm(oForm,obj);//serialize form voor submit
					$.submit({
						type 			: "post",
						url 			: oForm.action,
						onsuccess 		: this.submitSuccessResult,
						onfailure 		: this.submitFailureResult,
						scope			: o //callbackscope
						
					});
				},
				
				submitSuccessResult : function(oResponse) {
					this.trigger( {//trigger event met custom object
						type		: 'frm_submit',
						sender		: oForm.id,
						results		: oResponse	
					});
					output.setResult(oResponse, MSG_SUCCESS);
				},
			
				submitFailureResult : function(oResponse) {
					//doError(); //acties onerror
					output.setResult(oResponse, MSG_ERROR);
				},
				
				//req 02 ------------------------------------------
				doSave:function(t) {
					
					var elemId=t.getAttribute('rel');
					bRequestActive=true;
					$.getForm(oForm,elemId);//sla element op voor submit
					$.submit({
						type 			: "post",
						url 			: 'xhr/element_save.php',
						onsuccess 		: this.saveSuccessResult,
						onfailure 		: this.saveFailureResult,
						scope			: o //callbackscope
				
					});
				},
				
				saveSuccessResult : function(oResponse) {
					this.trigger( {//trigger event met custom object
						type		: 'frmfield_save',
						sender		: oForm.id,
						results		: oResponse					
					});
					output.setResult(oResponse, MSG_SUCCESS);
					$.echo(oResponse, true,'form.js saveSuccessResult regel 105',10);//logger
				},
			
				saveFailureResult : function(oResponse) {
					//doError(); //acties onerror
					output.setResult(oResponse, MSG_ERROR);
				},
				
				//req 03 ----------------------------------------------
				addPollUpdater:function() {
					
					$.getForm(oForm);//sla element op voor submit
					$.addPoll({
						type 			: "post",
						url 			: 'xhr/getpolldata.php',
						onsuccess 		: this.pollSuccessResult,
						onfailure 		: this.pollFailureResult,
						scope			: o //callbackscope
				
					});
				},
				
				pollSuccessResult : function(oResponse) {
					this.trigger( {//trigger event met custom object
						type		: 'poll_update',
						sender		: oForm.id,
						results		: oResponse,
						success		: true
					});
				},
			
				pollFailureResult : function(oResponse) {
					this.trigger( {//trigger event met custom object
						type		: 'poll_update',
						sender		: oForm.id,
						results		: oResponse,
						success		: false
					});
				},
				
				//req 04 --------------------------------------------------
				addPollDummy:function() {
					$.addPoll({
						type 			: "get",
						url 			: 'xhr/dummy.php?dummy=yes'
				
					});
				}
				
			};
			
			//PUBLIC INTERFACE-----------------------------------
			var o={
				identifier : 'frm',
				
				init : function(){
				
					oForm=$.getRefForm(0);//haal referentie naar formulier op
					$(oForm).bind('click.frm', handler.process); //namespacing click-event
				},
				
				frmSubmit:function(obj){
					if(bRequestActive){
						setTimeout(o.frmSubmit,200);//wacht op eventuele andere XHR-requests
					}else {
						XHR.doSubmit(obj);
					}
				},
				
				frmElementSave:function(t){
					if(bRequestActive){
						setTimeout(o.frmElementSave,50);//wacht op eventuele andere XHR-requests
					}else {
						XHR.doSave(t);
					}
				},
				
				startPolling:function(interval){
					$.setPollingInterval(interval);
					XHR.addPollUpdater();
					$.startPolling();
				},
				
				addPollDummy:function(){
					
					XHR.addPollDummy();
				}
				
			};
			$.extend(o,oConnect);//custom events connector
			return o;
			//--------------------------------------------------
		})();
		
//TEST OBJECT		
var dummy = (function(){
	//PRIVATE CONSTANTS
	
	//PRIVATE VARS
	var oConnect = new objEventTarget();//event-interface object
	
	//PRIVATE METHODS
	
	//PUBLIC INTERFACE--------------------------------
	var o = {
		identifier : 'dummy',
		
		shoot : function(elem){
		
			this.trigger({//trigger event met custom object
						type		: 'frmfield_save',
						sender		: 'shoot '+elem.value,
						results		: {data : 'aaldert'},
						success		: true
					});
			//een testje		
			$('body').append('<div id="test">'+elem.value+'</div>');
			var oCss={ 
				'background-color':'red',
				'height':'80px',	
				'width':'120px',
				'display':'none'
			};
			$('#test').css(oCss).slideDown('slow').click(function(){
				$(this).fadeOut('slow',function(){
					$(this).detach();
				});
				
			});
			
		}
	
	};
	$.extend(o,oConnect);//custom events connector
	return o;

})();

//MODULES=======================================================
		
var UpdaterOne = (function(){
	var o = {
		receiver : 'UPDATERone',
		handler	: function(event){
			event.receiver=o.receiver;
			customAlert(event);//test	
		}	
	};
	return o;
})();

var UpdaterTwo = (function(){
	var o = {
		receiver : 'UPDATERtwo',
		handler	: function(event){
			event.receiver=o.receiver;
			customAlert(event);//test	
		}	
	};
	return o;
})();

var PollUpdater = (function(){
	var o = {
		receiver : 'POLLUPDATER',
		handler	: function(event){
			event.receiver=o.receiver;
			$('#alert').html('een test : '+ event.results.data + ' success : ' + event.success);//test
			$.doShow('alert',1000);
		}
	};
	return o;
})();

var DataProcessor = (function(){
	var o = {
		receiver : 'DATAPROCESSOR',
		handler	: function(event){
			//pickup event
			event.receiver=o.receiver;
			customAlert(event);//test		
		}	
	};
	return o;
})();

//START===========================================================

		$(document).ready(function(){
		
			$.extend(Util);//tools object
			$.extend(RequestManager);//xhr object
			
			frm.init();
			
			//interface
			frm.addListener('frm_submit', DataProcessor.handler);
			frm.addListener('frmfield_save', UpdaterOne.handler);
			frm.addListener('frmfield_save', UpdaterTwo.handler);
			frm.addListener('poll_update', PollUpdater.handler);
			dummy.addListener('frmfield_save', UpdaterOne.handler);
			
			frm.startPolling(4000);
			$.logInit();//logger, arg=true then relative, arg='console' then output to firebug console
		});
		
//================================================================

		
//helper testing
function customAlert(event){
	alert('receiver : '+event.receiver +', \ntype: ' + event.type + ',  \nresults: ' + event.results.data + ', \nsender : ' + event.sender + ', \nidentifier : '+ event.target.identifier);
}