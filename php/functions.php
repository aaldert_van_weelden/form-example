<?php
	function setAjaxSearch($p_sOmschrijving='', $p_sName='', $p_sValue='', $p_sVerplicht='', $p_sSize='40', $p_sSubject='one', array $p_aVars, $p_sPath='', $p_iTop=0, $p_iHorPos=0, $p_sTarget='', $p_iLevel=0, $p_bEcho=false, $p_sTabel='',$p_sRow='',$p_sFirstID='',$p_bWrap=true) {//array $p_aVars( arg1=>var1, arg2=>var2 etc etc )
		//$p_sTabel bevat informatie over de te raadplegen database-tabel, gemapt in config/rubriek_config.php
		
		
		$str_vars='&sub='.$p_sSubject;
		$str_vars.='&name='.$p_sName;
		$str_vars.='&size='.$p_sSize;
		$str_vars.='&path='.$p_sPath;
		$str_vars.='&target='.$p_sTarget;
		$str_vars.='&level='.$p_iLevel;
		$str_vars.='&row='.$p_sRow;
		$str_vars.='&value='.$p_sValue;
		
		empty($p_sTabel) ? $str_vars.='&tabel='.$p_sSubject : $str_vars.='&tabel='.$p_sTabel;
		
		$pos='left';
		
		foreach($p_aVars as $varnaam => $varvalue) {
			$str_vars.='&';
			$str_vars.=$varnaam;
			$str_vars.='=';
			$str_vars.=$varvalue;
		}
		$m_sFormulier='';
		
		if($p_bWrap) {
		$m_sFormulier		.='
		
		<ul class="no_list">
			<li class="omschrijving">'.$p_sOmschrijving.'</li>
			<li class="inhoud"><div id="cnt_'.$p_sSubject.$p_sRow.'"><input type="text" disabeled="disabeled" readonly="readonly" name="'.$p_sName.$p_sRow.'" value="'.$p_sValue.'" id="txt_'.$p_sSubject.$p_sRow.'" size="'.$p_sSize.'"></input><input type="hidden" value="'.$p_sFirstID.'" name="id_'.$p_sRow.'"></input></div></li>
			<li class="verplicht"><div class="btn_order" onClick="toggle(\''.$p_sSubject.$p_sRow.'_search\');xmlhttpPost1(\''.$p_sPath.'ajax_'.$p_sSubject.'_info.php\',\'q='.$str_vars.'\',\''.$p_sSubject.$p_sRow.'_select\')"><img class="ajax_dropdown" alt="Selecteer '.$p_sSubject.$p_sRow.'" src="'.$p_sPath.'images/dropdown.gif"></div></li>
			<li class="verplicht">'.$p_sVerplicht.'</li>
				</ul>
          		<div class="ajax_result" style="display:none;'.$pos.':'.$p_iHorPos.'px;top:'.$p_iTop.'px;" id="'.$p_sSubject.$p_sRow.'_search">
		  		<input class="text" id="keystring'.$p_sRow.'" value="" autocomplete="off" onkeyup=\'JavaScript:xmlhttpPost1("'.$p_sPath.'ajax_'.$p_sSubject.'_info.php","q=" + this.value + "'.$str_vars.'","'.$p_sSubject.$p_sRow.'_select")\' type="text" name="string'.$p_sRow.'" ><img class="zoek" src="'.$p_sPath.'images/find.png" /><img onclick="toggle(\''.$p_sSubject.$p_sRow.'_search\');" class="close" src="'.$p_sPath.'images/close.gif" />
		  			<div id="'.$p_sSubject.$p_sRow.'_select">
		  			</div>
		  		</div>
		  		<div  id="'.$p_sSubject.$p_sRow.'_info">
			  		<div id="'.$p_sSubject.$p_sRow.'">		  		
			  		</div>
		  		</div>';
		}else {
			$m_sFormulier		.='
		
		
			
			<div id="cnt_'.$p_sSubject.$p_sRow.'"><input type="text" disabeled="disabeled" readonly="readonly" name="'.$p_sName.$p_sRow.'" value="'.$p_sValue.'" id="txt_'.$p_sSubject.$p_sRow.'" size="'.$p_sSize.'"></input><input type="hidden" value="'.$p_sFirstID.'" name="id_'.$p_sRow.'"></input></div>
			<div class="btn_order" onClick="toggle(\''.$p_sSubject.$p_sRow.'_search\');xmlhttpPost1(\''.$p_sPath.'ajax_'.$p_sSubject.'_info.php\',\'q='.$str_vars.'\',\''.$p_sSubject.$p_sRow.'_select\')"><img class="ajax_dropdown_nowrap" alt="Selecteer '.$p_sSubject.$p_sRow.'" src="'.$p_sPath.'images/dropdown.gif"></div>
          		<div class="ajax_result" style="display:none;'.$pos.':'.$p_iHorPos.'px;top:'.$p_iTop.'px;" id="'.$p_sSubject.$p_sRow.'_search">
		  		<input class="text" id="keystring'.$p_sRow.'" value="" autocomplete="off" onkeyup=\'JavaScript:xmlhttpPost1("'.$p_sPath.'ajax_'.$p_sSubject.'_info.php","q=" + this.value + "'.$str_vars.'","'.$p_sSubject.$p_sRow.'_select")\' type="text" name="string'.$p_sRow.'" ><img class="zoek" src="'.$p_sPath.'images/find.png" /><img onclick="toggle(\''.$p_sSubject.$p_sRow.'_search\');" class="close" src="'.$p_sPath.'images/close.gif" />
		  			<div id="'.$p_sSubject.$p_sRow.'_select">
		  			</div>
		  		</div>
		  		<div  id="'.$p_sSubject.$p_sRow.'_info">
			  		<div id="'.$p_sSubject.$p_sRow.'">		  		
			  		</div>
		  		</div>';
			
		}
	if($p_bEcho) {
			echo $m_sFormulier;
		} else {
			return $m_sFormulier;
		}
	}



?>